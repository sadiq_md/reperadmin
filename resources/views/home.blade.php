@extends('layouts.app')

@section('content')

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

<div class="container-fluid">
    <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="navbar-toggler">
                        <a class="nav-link" data-toggle="collapse" data-target="#dashboard" aria-expanded="false" aria-controls="dashboard">
                            <span class="menu-title">Dashboard</span>
                        </a>
                        <div class="collapse" id="dashboard">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item"> <a class="nav-link" href="#">Usage Statistics</a></li>
                                <li class="nav-item"> <a class="nav-link" href="#">Run Background Check</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container">
        <h4>Run Background</h4>
        <form method="GET" action="{{route('dashboard.run')}}" autocomplete="off" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{--        {{ Form:: }}--}}
                <div class="col-md-6">
                    <div class="form-group {{ $errors-> has('name') ? 'has-error' : ''}}">
                        <label for="first_name">Name</label>
                        <input type="text" id="name" name="name" class="form-control"
                               value="{{ old('name') }}">
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors-> has('user_name') ? 'has-error' : ''}}">
                        <label for="user_name">UserName</label>
                        <input type="text" id="user_name" name="username" class="form-control"
                               value="{{ old('name') }}">
                        <span class="text-danger">{{ $errors->first('user_name') }}</span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors-> has('date_of_birth') ? 'has-error' : ''}}">
                        <label for="date_of_birth">Date Of Birth</label>
                        <input type="text" id="date_of_birth" name="dob" class="form-control"
                               value="{{ old('date_of_birth') }}">
                        <span class="text-danger">{{ $errors->first('date_of_birth') }}</span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-primary" value="@lang('driver.submit')">
                </div>
        </form>
    </div>
@endsection
